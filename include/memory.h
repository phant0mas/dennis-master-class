// This file is published under the MIT License
// Copyright 2018 Manolis Fragkiskos Ragkousis
// Check the license file.

#ifndef MEMORY_H
#define MEMORY_H

#include <cstdint>

/** A very thin memory wrapper */
class memory_block {
private:
  uint8_t *block;  /**< Pointer to the allocated bytes */
  uint32_t block_size;  /**< Size of allocated bytes */

public:
  /*
   * Constructor() - Allocates a memory block of n bytes
   *
   * @param n - The size in bytes of the allocated memory block.
   *
   */
  explicit memory_block(std::size_t n);
  /*
   * Destructor() - Frees the allocated memory
   */
  ~memory_block();
  /*
   * Copy constructor
   *
   * @param other - Value to init with
   */
  memory_block(memory_block const &other);
  /*
   * Copy assignment operator
   *
   * @param rhs - Right hand operator
   */
  memory_block & operator=(memory_block const &rhs);
  /*
   * @return - The size in bytes of the memory block
   */
  std::size_t size() const;
  /*
   * data() - Returns a (raw) pointer to the allocated memory block,
   * so we can read/write from/into it
   *
   * @return - A pointer to the allocated space
   */
  void *data();
  void const *data() const;
};

#endif /* MEMORY_H */
