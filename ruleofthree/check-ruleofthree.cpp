// This file is published under the MIT License
// Copyright 2018 Manolis Fragkiskos Ragkousis
// Check the license file.

#include <memory.h>
#include <cstdint>
#include <string>
#include <cstdio>

using namespace std;

int main(int argc, char** argv) {
  memory_block mblock(20);
  memory_block mblock2(20);
  std::string str;
  std::size_t str_len = 0;
  char *mptr = (char *) mblock.data();

  str = std::string("Hello Dennis\n");
  str_len = str.copy(mptr, str.length(), 0);

  printf("Written %ld bytes to memory block\n", str_len);
  printf("Reading string from memory block 1: %s", mptr);

  mblock2 = mblock;
  mptr = (char *) mblock2.data();

  printf("Written %ld bytes to memory block, after copy assignment\n", str_len);
  printf("Reading string from memory block 2: %s", mptr);

  memory_block mblock3(20) = mblock;
  mptr = (char *) mblock2.data();

  printf("Written %ld bytes to memory block, after copy constructor\n", str_len);
  printf("Reading string from memory block 2: %s", mptr);

  return 0;
}
