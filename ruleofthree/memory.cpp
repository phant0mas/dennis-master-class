// This file is published under the MIT License
// Copyright 2018 Manolis Fragkiskos Ragkousis
// Check the license file.

#include <memory.h>
#include <cstring>

memory_block::memory_block(std::size_t n) {
  memory_block::block = 0;
  memory_block::block = new uint8_t[n];

  if (memory_block::block) {
    memory_block::block_size = n;
  }
}

memory_block::~memory_block() {
  delete[] memory_block::block;
}

memory_block::memory_block(memory_block const & other) {
  std::memcpy(memory_block::block, other.data(), other.size());
}

memory_block & memory_block:: operator=(memory_block const & rhs) {
  std::memcpy(memory_block::block, rhs.data(), rhs.size());
  block_size = rhs.size();
  return *this;
}

std::size_t memory_block::size() const {
    return memory_block::block_size;
}

void *memory_block::data() {
  return (void *) memory_block::block;
}

void const *memory_block::data() const {
  return (void *) memory_block::block;
}
