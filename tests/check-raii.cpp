// This file is published under the MIT License
// Copyright 2018 Manolis Fragkiskos Ragkousis
// Check the license file.

#include <memory.h>
#include <cstdint>
#include <string>
#include <cstdio>

using namespace std;

int main(int argc, char** argv) {
  memory_block mblock(20);
  std::string str;
  std::size_t str_len = 0;
  char *mptr = (char *) mblock.data();

  str = std::string("Hello Dennis\n");
  str_len = str.copy(mptr, str.length(), 0);
  printf("Written %ld bytes to memory block", str_len);
  printf("Reading string from memory block: %s", mptr);

  return 0;
}
