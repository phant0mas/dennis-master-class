C++ Master Class
=====================

# Dependencies 

On a GuixSD system (or with just Guix):  
``` bash 
guix package -i gcc-toolchain make
```

On a Debian based system:  

``` bash
sudo apt install build-essential make
```

# Building 

Enter the directory which you are interested and run

``` bash
make
```

If you want to build it from the root directory, just run

``` bash
make -C $dirname
```

where `$dirname` the directory you want.

# Testing RAII

``` bash
make -C raii
make -C tests
./tests/check-raii
```

